## Aplicativo Web para Gerenciar Informações de Clientes com Django e React

This is a Django project with a React front-end.

The REST API is created with Django REST framework and consumed with Axios.


Aplicativo Web com uma API REST separada com back-end e front-end, usando React, Django e o Django REST Framework. Ao usar o React com o Django, conseguimos nos beneficiar dos últimos avanços em JavaScript e desenvolvimento front-end. Em vez de construir um aplicativo Django que utilize um mecanismo de template interno, usamos o React como uma biblioteca de UI, beneficiando-nos de sua abordagem informativa virtual Modelo de Documento por Objetos (do inglês Document Object Model - DOM) e de componentes que processam rapidamente alterações nos dados.


## Algumas ferramentas que usamos ao construir o aplicativo incluem:

O React, um framework JavaScript que permite que os desenvolvedores construam front-ends para Web e nativos para seus back-ends de API REST.

O Django, um framework Web gratuito e de código aberto, escrito em Python, que segue o padrão arquitetônico de software Model View Controller (MVC) [Modelo-Visão-Controle].

Framework Django REST, um kit de ferramentas eficaz e flexível para a criação de APIs REST no Django

## A CustomerList Component

## A CustomerCreateUpdate Component